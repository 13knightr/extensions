#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) 2009 Aurelio A. Heckert, aurium (a) gmail dot com
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

import os
import re
import sys

from lxml import etree

import inkex

class InkWebEffect(inkex.EffectExtension):
    def __init__(self):
        super(InkWebEffect, self).__init__()
        self.reUpdateJS = '/\\*\\s* inkweb.js [^*]* InkWebEffect:AutoUpdate \\s*\\*/'

    def effect(self):
        pass

    def mustAddInkWebJSCode(self, scriptEl):
        if not scriptEl.text:
            return True
        if len(scriptEl.text) == 0:
            return True
        if re.search(self.reUpdateJS, scriptEl.text):
            return True
        return False

    def addInkWebJSCode(self, scriptEl):
        with open(os.path.join(sys.path[0], "inkweb.js")) as js:
            scriptEl.text = etree.CDATA("\n/* inkweb.js - InkWebEffect:AutoUpdate */\n" + js.read())

    def ensureInkWebSupport(self):
        # Search for the script tag with the inkweb.js code:
        scriptEl = None
        scripts = self.svg.xpath('//svg:script')
        for s in scripts:
            if re.search(self.reUpdateJS, s.text):
                scriptEl = s

        if scriptEl is None:
            root = self.document.getroot()
            scriptEl = etree.Element("script")
            scriptEl.set("id", "inkwebjs")
            scriptEl.set("type", "text/javascript")
            root.insert(0, scriptEl)

        if self.mustAddInkWebJSCode(scriptEl):
            self.addInkWebJSCode(scriptEl)
